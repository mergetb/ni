from typing import List, Dict, Any, Callable, Union, Optional
from xir import Network
from svg import Shape, Canvas, Viewport, Point2, Zoom
from copy import deepcopy
from svgwrite import Drawing

class View:
    canvas: Canvas
    viewport: Viewport

    def __init__(self, 
                 width: int = 500, 
                 height: int = 500, 
                 zoom: Zoom = Zoom(5)) -> None:
        self.canvas = Canvas()
        self.viewport = Viewport(zoom, Point2(0,0))

Selection = Dict[Shape, bool]

class StateSpace:
    shapes: List[Shape]
    view: View
    selection: Selection
    network: Network

    def __init__(self) -> None:
        self.shapes = []
        self.view = View()
        self.selection = Selection()
        self.Network = Network({})

Modifier   = Callable[[StateSpace], Network]
Abstractor = Callable[[StateSpace], List[Shape]]
Aperature  = Callable[[StateSpace], View]
Selector   = Callable[[StateSpace], Selection]
BaseFn     = Union[Modifier, Abstractor, Aperature, Selector]

class Ni:
    state: List[StateSpace]
    chain: List[BaseFn]

    def __init__(self) -> None:
        self.state.append(StateSpace())
    
    # Base function acceptors
    def modify(self, fn: Modifier):
        new = deepcopy(self.state[-1])
        new.network = fn(new)
        self.chain.append(fn)
        self.state.append(new)
        self.render()

    def abstract(self, fn: Abstractor):
        new = deepcopy(self.state[-1])
        new.shapes = fn(new)
        self.chain.append(fn)
        self.state.append(new)
        self.render()

    def view(self, fn: Aperature):
        new = deepcopy(self.state[-1])
        new.view = fn(new)
        self.chain.append(fn)
        self.state.append(new)
        self.render()

    def select(self, fn: Selector):
        new = deepcopy(self.state[-1])
        new.selection = fn(new)
        self.chain.append(fn)
        self.state.append(new)
        self.render()

    def render(self) -> str:
        drawing = Drawing('drawing.svg', profile='full')
        for x in self.state[-1].shapes:
            x.draw(drawing)
        return drawing.toString()

