from typing import List, Tuple
from svgwrite import Drawing


class Canvas:
    width: int
    height: int
    
    def __init__(self) -> None:
        self.width = 500
        self.height = 500

class Point2:
    x: int
    y: int

    def __init__(self, x: int, y: int) -> None:
        self.x = x
        self.y = y

class Zoom:
    value: int

    def __init__(self, value: int = 5) -> None:
        if value > 5:
            raise ValueError("zoom out of range, must be 0 >= z <= 5")
        if value < 0:
            raise ValueError("zoom out of range, must be 0 >= z <= 5")

        self.value = value

class Viewport:
    zoom: Zoom
    center: Point2

    def __init__(self, zoom: Zoom, center: Point2) -> None:
        self.zoom = zoom
        self.center = center

class Shape:
    uuid: str

    def __init__(self) -> None:
        pass

    def draw(self, d: Drawing) -> None:
        pass

class Circle(Shape):
    radius: int
    def __init__(self, radius:int=10) -> None:
        self.radius = radius

    def draw(self, d: Drawing) -> None:
        d.add(d.circle(r=self.radius))

class Rect(Shape):
    def __init__(self) -> None:
        pass

    def draw(self, d: Drawing) -> None:
        #TODO
        pass

class Line(Shape):
    start: Tuple[int, int]
    end:   Tuple[int, int]

    def __init__(self, start: Tuple[int,int], end: Tuple[int,int]) -> None:
        self.start = start
        self.end = end

    def draw(self, d: Drawing) -> None:
        d.add(d.line(start=self.start, end=self.end))

class Group(Shape):
    children : List[Shape]

    def __init__(self) -> None:
        pass

    def draw(self, d: Drawing) -> None:
        for x in self.children:
            x.draw(d)
