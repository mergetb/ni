#!/bin/bash

# python server side
python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. ni.proto

# javascript client side

rm -rf ni-app/src/proto/*
protoc -I. ni.proto \
  --js_out=import_style=commonjs,binary:ni-app/src/proto \
  --grpc-web_out=import_style=commonjs,mode=grpcwebtext:ni-app/src/proto

echo "/* eslint-disable */" > /tmp/out 
cat ni-app/src/proto/ni_pb.js >> /tmp/out 
mv /tmp/out ni-app/src/proto/ni_pb.js

echo "/* eslint-disable */" > /tmp/out 
cat ni-app/src/proto/ni_grpc_web_pb.js >> /tmp/out 
mv /tmp/out ni-app/src/proto/ni_grpc_web_pb.js
