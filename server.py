#!/usr/bin/python3

from typing import Dict, Any
import grpc
import time
from concurrent import futures
import ni_pb2_grpc as rpc
import ni_pb2 as proto

class NiServicer(rpc.NiServicer):

    def __init__(self) -> None:
        pass

    def Modify(self, request, context) -> Any:
        return newVDiff()

    def Abstract(self, request, context) -> Any:
        return newVDiff()

    def View(self, request, context) -> Any:
        return newVDiff()

    def Select(self, request, context) -> Any:
        return newVDiff()

    def Health(self, request, context) -> Any:
        return proto.HealthStatus(
            message="ok"
        )


#TODO the return value from proto.VDiff is not a class, so i'm not sure how
#     to attach a type hint atm
def newVDiff() -> Any:
    shape_diff = proto.ShapeDiff(
        add = [],
        delete = [],
        modify = []
    )

    view = proto.View(
        viewport=proto.Viewport(
            zoom=5,
            center=proto.Point2(x=0, y=0)
        ),
        canvas=proto.Canvas(
            width=500,
            height=500
        )
    )

    selection : Dict[str,bool] = {} 

    return proto.VDiff(
        shapes=shape_diff,
        view=view,
        selection=selection
    )

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    rpc.add_NiServicer_to_server(
        NiServicer(), server
    )
    server.add_insecure_port('[::]:6000')
    server.start()
    try:
        while True:
            time.sleep(100000)
    except KeyboardInterrupt:
        server.stop(0)

if __name__ == '__main__':
    serve()
